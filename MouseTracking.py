__author__ = 'yudelin'

import tkinter as tk
import LSE


class MousePositionTracking(LSE.LeastSquareEstimation):

    recorded = 0
    max_record = 100
    y_recorded_list = []
    x_recorded_list = []
    # Use two empty lists to store data
    # 100 positions for LSE training
    # Instance variables are existing for instance.
    # In other words,
    # different objects from the same class have their own instance variables
    # Class variables are shared by all objects.
    #-----------------------------------------------------------------------------------------------

    def __init__(self):
        super().__init__()
        self.root = tk.Tk()
        self.my_canvas = tk.Canvas(self.root, width=700, height=500)
        # Create canvas for marking the position
        #-----------------------------------------------------------------------------------------------
        self.px1, self.py1 = 0, 0
        self.px2, self.py2 = 0, 0
        # Initial the parameters of the oval.
        #-----------------------------------------------------------------------------------------------

    def start(self):
        self.my_canvas.bind("<Motion>", self.combine_functions)
        self.my_canvas.pack()
        self.my_canvas.mainloop()
        #-----------------------------------------------------------------------------------------------
        # Independent the start function from the __init__

    def show_mouse_position_on_windows_title(self, event):
        str1 = "mouse at x = %d, y = %d" % (event.x, event.y)
        # for i in range(1, printself.recorded):
        self.root.title(str1)

    def record_100_mouse_position(self, event):
        if self.recorded < self.max_record:
            self.x_recorded_list += [event.x]
            self.y_recorded_list += [event.y]
            # I use list instead of numpy's array because I am not so familiar with numpy's array :p
            self._print_records()
        self.recorded += 1
        # Keep recording data even until the process has exited

    def mark_mouse_position(self, event):
        self.px1, self.py1 = (event.x - 2), (event.y - 2)
        self.px2, self.py2 = (event.x + 2), (event.y + 2)
        # size of the oval which mark the positions of the mouse
        self.my_canvas.create_oval(self.px1, self.py1, self.px2, self.py2, fill = "red")
        # mapping the position on the canvas

    def _print_records(self):
        print("Position = %d" % (self.recorded + 1))
        print("x = %d, y = %d "
              % (self.x_recorded_list[self.recorded],
                 self.y_recorded_list[self.recorded]))
        if self.recorded == 100:
            for i in range(0, 100):
                print("x[%d] = %d, y[%d] = %d"
                      % ((i + 1), self.x_recorded_list[i],
                         (i + 1), self.y_recorded_list[i]))

    def __do_LSE__(self):
        if self.recorded == 100:
            super().start_lse(self.x_recorded_list, self.y_recorded_list)
            # self.start_lse(self.x_recorded_list, self.y_recorded_list)

    def __mouse__tracking__(self, event):
        if self.recorded > 100:
            theta0, theta1 = self.return_theta_value()
            y = theta0 + theta1 * (event.x / 700)
            y *= 500
            # The results we get have been scaled,
            # hence, we have to scale back.
            px1, py1 = event.x - 2, y - 2
            px2, py2 = event.x + 2, y + 2
            self.my_canvas.create_oval(px1, py1, px2, py2, fill = "blue")
        # Predict the positions

    def combine_functions(self, event):
        self.show_mouse_position_on_windows_title(event)
        self.mark_mouse_position(event)
        self.record_100_mouse_position(event)
        self.__do_LSE__()
        self.__mouse__tracking__(event)
        # self.__quit__()

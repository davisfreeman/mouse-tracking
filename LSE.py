__author__ = 'yudelin'

from random import uniform


class LeastSquareEstimation():

    sample_numbers = 100
    # sample_number we used for training
    learning_rate = 0.05
    # learning rate for cost function

    def __init__(self):
        self.theta0 = 0
        self.theta1 = 0
        # hypothesis: y = theta0 + theta1 * x
        #-----------------------------------------------------------------------------------------------
        self.result = []
        # result records three data for every hypothesis function,
        # result[0] records the cost value
        # result[1] records the theta0 and
        # result[2] records the theta1
        #-----------------------------------------------------------------------------------------------
        self.x_data_list = []
        self.y_data_list = []
        # Initialize the x, y data list inside the LSE.
        #-----------------------------------------------------------------------------------------------

    def __randomize__(self):
        self.theta0 = uniform(0, 5)
        self.theta1 = uniform(0, (5/7))

    def __features_scaling__(self, x_data_list, y_data_list):
        for i in range(0, 100):
            x_data_list[i] /= 700
            y_data_list[i] /= 500
        # x_data_list & y_data_list are called by referenced.
        # Feature scaling can reduce the calculation.
        self.x_data_list = x_data_list
        self.y_data_list = y_data_list
        # Put the scaled data inti the list of the LSE class.

    def __cost_function__(self):
        tmp_sum = 0
        for i in range(0, self.sample_numbers - 1):
            tmp_sum += (self.theta0 + self.theta1 *
                        self.x_data_list[i] - self.y_data_list[i]) ** 2
            tmp_sum /= (2 * self.sample_numbers)
            return tmp_sum

    def __gradient_descent__(self):
        tmp_sum0 = 0
        tmp_sum1 = 0
        j_min = 1
        min_theta0 = self.theta0
        min_theta1 = self.theta1
        for i in range(0, self.sample_numbers - 1):
            tmp_sum0 += (self.theta0 + self.theta1 *
                         self.x_data_list[i] - self.y_data_list[i])
            tmp_sum1 += (self.theta0 + self.theta1 *
                         self.x_data_list[i] - self.y_data_list[i]) * self.x_data_list[i]
            self.theta0 -= (self.learning_rate * tmp_sum0 / self.sample_numbers)
            self.theta1 -= (self.learning_rate * tmp_sum1 / self.sample_numbers)
            j = self.__cost_function__()
            if j < j_min:
                j_min, min_theta0, min_theta1 = j, self.theta0, self.theta1
        return (j_min, min_theta0, min_theta1)

    def __record_result__(self, val_tuple):
        self.result += [(val_tuple)]
        # Record 100 minimum value of cost for 100 start points

    def __print_result__(self):
        print(self.result)

    def start_lse(self, x_data_list, y_data_list):
        self.__features_scaling__(x_data_list, y_data_list)
        # Do features scaling to make sure the data will not be out of control
        for i in range(0, 100):
            self.__randomize__()
            # We create 100 initial hypothesis functions
            for j in range(0, 100):
                tmp_result = self.__gradient_descent__()
            # For every hypothesis function we do 100 gradient descent
            self.__record_result__(tmp_result)
            # Record the minimum of the cost and theta0, theta1 when the minimum cost was found
        self.__print_result__()

    def return_theta_value(self):
        j_min = min(self.result)
        return j_min[1], j_min[2]
        # the theta0 and theta1 which have the minimum cost.